#include "MeuAgentePrincipal.h"
#include <AgentePrincipal.h>
#include <Windows.h>
#include <BWAPI\Position.h>
#include <BWAPI\UnitType.h>
#include <deque>

//blackboard!
bool GameOver = false;
std::deque<BWAPI::UnitType> buildings_list;
Unidade* mainBuilder = NULL;
//int workers = 0;
//

void AITrabalhador (Unidade* u){
	double distance = 99999;
	Unidade* mineralPerto = NULL;
	std::set<Unidade*> minerais = u->getMinerals();
	for(std::set<Unidade*>::iterator it = minerais.begin(); it != minerais.end(); it++){
		if(u->getDistance(*it) < distance){
			distance = u->getDistance(*it);
			mineralPerto = *it;
		}//vai minerar no mineral mais perto
	}
	if(mineralPerto != NULL) u->rightClick(mineralPerto);
}

bool BuilderAI(BWAPI::UnitType type) {
	BWAPI::TilePosition tp = mainBuilder->getTilePosition();
	int limite = 0;
	int adi = 6;
	bool not_built = true;
	while(not_built = !(mainBuilder)->build(tp, type)) {
		if(((mainBuilder->minerals()&0xF)>>2) < 2) 
			tp = BWAPI::TilePosition(tp.x(), tp.y()+adi);
		else
			tp = BWAPI::TilePosition(tp.x()+adi, tp.y());
		tp.makeValid();
		limite++;
		if(limite > 50) break;
		adi = -adi + (adi > 0 ? -2 : +2);
	}
	return !not_built;
}

void AICentroComando (Unidade* u){
	std::set<Unidade*> amigos = u->getAllyUnits();
	int workers = 0;
    for(std::set<Unidade*>::iterator it = amigos.begin(); it != amigos.end(); it++){
		if (GameOver) return;
		if ((*it)->getType().isWorker()) ++workers;
	}
	if (workers < 7) {
		// Se for command center e tiver menos de 7 SCVs, faz SCV
		u->train(BWAPI::UnitTypes::Terran_SCV);
	}

	// Se for barracks, faz marines
	u->train(BWAPI::UnitTypes::Terran_Marine);
}

void AIGuerreiro (Unidade* u){
	// Checa se o jogo acabou e se existem unidades inimigas ainda
	if (GameOver) return;
	if (!u->getEnemyUnits().empty()) {
		u->attack(*(u->getEnemyUnits().begin()));
	}
}

DWORD WINAPI threadAgente(LPVOID param){
	
	Unidade *u = (Unidade*) param;

	//exemplos de metodos uteis para construir predios
	/*bool x = u->hasPower(3,4,50,60);
	u->isBuildable(50,50);
	u->isBuildable(BWAPI::TilePosition(3,5));*/

	/*int heig = AgentePrincipal::mapHeight();
	int wid = AgentePrincipal::mapWidth();*/
	//A classe Agente Principal ainda tem o metodo AgentePrincipal bool isWalkable(int x, int y).

	while(true){
		//Se houve algum problema (ex: o jogo foi fechado) ou a unidade estah morta, finalizar a thread
		if(GameOver || u == NULL || !u->exists()) return 0;
		//Enquanto a unidade ainda nao terminou de ser construida ou o seu comando ainda nao foi
		//processado (ou seja, continua no mesmo turno), aguardar e poupar processamento
		if(!u->isCompleted()){
			Sleep(500);
			continue;
		}
		if(!u->checkNovoTurno()){
			Sleep(10);
			continue;
		}

		//Inserir o codigo de voces a partir daqui//
		if(u->isIdle()){ //nao ta fazendo nada, fazer algo util
			if (mainBuilder == NULL && u->getType().isWorker()) mainBuilder = u;
			if(u->getType().isWorker()) AITrabalhador(u);
			else if(u->getType().canProduce()) AICentroComando(u);
			else AIGuerreiro(u);
		}
		Sleep(10);//Sempre dormir pelo menos 10ms no final do loop, pois uma iteracao da thread � muito mais r�pida do que um turno do bwapi.
	}
}

DWORD WINAPI threadMainAgent(LPVOID param){
	// Build buildings list
	buildings_list.push_back(BWAPI::UnitTypes::Terran_Supply_Depot);
	buildings_list.push_back(BWAPI::UnitTypes::Terran_Barracks);
	buildings_list.push_back(BWAPI::UnitTypes::Terran_Bunker);

	while(true) {
		//Lembrar que ha varias threads rodando em paralelo. O erro presente neste metodo (qual?) nao resulta em crash do jogo, mas outros poderiam.
		if (GameOver) return 0;

		if (mainBuilder) {
			if (!buildings_list.empty() && mainBuilder->minerals() >= buildings_list.front().mineralPrice()) {
				bool built = BuilderAI(buildings_list.front());
				if (built) {
					buildings_list.pop_front();
				}
			}
		}

		Sleep(10);//Sempre dormir pelo menos 10ms no final do loop, pois uma iteracao da thread � muito mais r�pida do que um turno do bwapi.
	}	
	
}


void MeuAgentePrincipal::InicioDePartida(){
	//Inicializar estruturas de dados necessarias, ou outras rotinas de inicio do seu programa. Cuidado com concorrencia, 
	//em alguns casos pode ser recomendavel que seja feito antes do while na criacao de uma nova thread.
	GameOver = false;
	CreateThread(NULL, 0, threadMainAgent,NULL, 0, NULL);
}

void MeuAgentePrincipal::onEnd(bool isWinner){  
	//sinalizar e aguardar o tempo necessario para todas as threads se terminarem antes do jogo sair, evitando erros.
	GameOver = true;
	Sleep(750);
}

void MeuAgentePrincipal::UnidadeCriada(Unidade* unidade){
	//Uma nova unidade sua foi criada (isto inclui as do inicio da partida). Implemente aqui como quer tratar ela.
	BWAPI::UnitType tipo = unidade->getType();
	
	//Nao desperdicar threads com predios que nao fazem nada
	if(tipo != BWAPI::UnitTypes::Terran_Supply_Depot && tipo != BWAPI::UnitTypes::Terran_Refinery){
		CreateThread(NULL,0,threadAgente,(void*)unidade,0,NULL);
	}
	else{
		//talvez voce queira manter a referencia a estes predios em algum lugar
	}
}

/*
	Os outros eventos nao existem numa arquitetura orientada a Agentes Autonomos, pois eram relacionados ao Player do Broodwar
	de maneira generica, nao sendo especificamente ligados a alguma unidade do jogador. Se desejado, seus comportamentos podem
	ser simulados atrav�s de t�cnicas ou estruturas de comunica��o dos agentes, como por exemplo Blackboards.
*/


